package com.dna_analyzer.data.exceptions;

public class DNADataException extends Exception {
    public DNADataException() {
        super("Ocorreu um erro ao obter os dados");
    }

    public DNADataException(String msg) {
        super(msg);
    }
}
