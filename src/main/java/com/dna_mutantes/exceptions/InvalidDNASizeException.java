package com.dna_analyzer.exceptions;

public class InvalidDNASizeException extends Exception {
    public InvalidDNASizeException() {
        super("O tamanho do DNA e invalido. Deve ser NxN");
    }
}
