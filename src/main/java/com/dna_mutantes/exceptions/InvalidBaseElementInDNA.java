package com.dna_analyzer.exceptions;

public class InvalidBaseElementInDNA extends Exception {
    public InvalidBaseElementInDNA() {
        super("O DNA contem valores invalidos de nitrogenada");
    }
}
