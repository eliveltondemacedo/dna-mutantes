package com.dna_analyzer.data.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DNADataExceptionTest {
    @Test
    void defaultConstructorShouldSetDefaultMessage() {
        DNADataException exception = new DNADataException();
        assertEquals(exception.getMessage(), "Ocorreu um erro ao obter os dados");
    }

    @Test
    void stringParameterConstructorShouldSetCustomMessage() {
        DNADataException exception = new DNADataException("Mensagem personalizada");
        assertEquals(exception.getMessage(), "Mensagem personalizada");
    }
}